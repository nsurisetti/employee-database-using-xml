# This program demonstrates how to perfrom full CRUD using the etree XML library

import os
import xmlschema
import xml.etree.cElementTree as ET

def Employee_menu():
	print("")
	print("E M P L O Y E E  D A T A B A S E")
	print("0 -To find or to create the database and file")
	print("1 -Create a new employee")
	print("2 -update the details of an employee")
	print("3 -Delete an employee")
	print("4 -Search for an employee")
	print("5 -Show details of all employees")
	print("6 -Validation of the employee data")
	print("7 -Exit")
	user_input=input("Choose option 1-6 or '7' to exit: ")
	return user_input


def directory_check(dirpath):
  if os.path.exists(dirpath):
    print("Database exist")
    return dirpath
  else: #this is optional if you want to create a directory if doesn't exist.
    os.mkdir(dirpath)
    print("Database created")
    return dirpath


def file_check(file_path):
  if os.path.exists(file_path):
    print("file exist")
    return file_path
  else: #this is optional if you want to create a directory if doesn't exist.
    open(file_path,'x')
    print("file created")
    return file_path


def validate(filepath):
	my_schema = xmlschema.XMLSchema('xmlschema1.xsd')
	x = my_schema.is_valid(filepath)
	print(x)

def show_all_employees():
	for i in root.findall('employee'):
		empid= i.find('empid').text
		empname = i.find('empname').text
		department = i.find('department').text
		print("EMPID: "+ empid)        
		print("EmpName: "+ empname)
		print("Department: "+department)
	print("End of list")


def search_for_an_employee():
	#ask the user for the student id to search for
	empid1 =input("Enter the emp id you want to search for: ")
	
	#go through each record to find the record with the specified Email address
	for i in root.findall('employee'):
		empid2 = i.find('empid').text

		#is the rquested id the same as this records id?
		if empid2==empid1:
			#we have found a match get all the record field values
			empid = i.find('empid').text
			empname = i.find('empname').text
			department = i.find('department').text
			#print the field values
			print("EMPLOYEE RECORD ID: "+ empid)
			print("EMP ID: "+ empid)        
			print("Emp Name: "+ empname)
			print("Department: "+ department)

def recordExists(empid):
	#set the default return value to false
	return_value=False

	#go through each record to find the one with the specified Email address
	for i in root.findall('employee'):
		empid1 = i.find('empid').text
		#is the requested id the same as this records id?
		if empid1==empid:
			#we have found a match set the return value to true
			return_value=True
	#finally return if the record was found to the caller        
	return return_value


def update_employee():
	#ask the user for the student id to edit 
	empid1 =input("Enter the employee id of the employee to edit: ")

	#go through each record to find the record with the specified id
	for i in root.findall('employee'):
		empid2 = i.find('empid').text

		#test: is the rquested id the same as this records id?
		if empid2==empid1:
			#YES: we have found a match get all the record field values
			empid = i.find('empid').text
			empname = i.find('empname').text
			department = i.find('department').text

			#edit employee name
			empname = input("Please enter empname:(" + empname + ")") or empname
			i.find('empname').text= empname
			
			#edit department of employee
			department = input("Please enter department:(" + department + ")") or department
			i.find('department').text= department

			
			#save the update
			# ntree.write(file_check(file_path))
			ntree.write(whole_path)
			#print the field values
			print("EMPLOYEE ID: "+ empid + " UPDATED")

def newid():
	maxid = 0
	for i in root.findall('employee'):
		id= int(i.find('empid').text)
		if id>maxid:
				maxid=id
	return maxid+1


def create_employee():
	#add a record-but we would need to check for duplicates first
	#ask the user for the student email address to search for
	empid =input("Enter the Employee id of the employee you want to search for: ")    
	exists=recordExists(empid)
	if exists==False:
		#gt a new id
		nid=newid()

		#get the field values from the user
		print("Create a record")
		empname=input("Employee Name:")
		department=input("department:")

		#create a contact element at root level
		# newrecord = ET.SubElement(root, "employee",id=str(nid))
		newrecord = ET.SubElement(root, "employee")
		#add the fields into out new record
		ET.SubElement(newrecord, "empid").text = str(nid)
		ET.SubElement(newrecord, "empname").text = empname
		ET.SubElement(newrecord, "department").text = department

		#finally save the update
		# ntree.write(file_check(file_path))    
		ntree.write(whole_path)    

	else:
		print("Record already exists")

def delete_employee():

	#get a variable for the record that you want to delete
	deleterecord=input("Enter the Employee id of the record you want to delete: ")

	#OK, now loop through all records looking for specified email address
	for i in root.findall('employee'):
		#get the email of the current record, 
		empid= i.find('empid').text
		#check is this the email we are looking for?
		if empid == deleterecord:
			#YES: remove the record from the xml tree
			root.remove(i)            
			#finally save the update
			# ntree.write(file_check(file_path))   
			ntree.write(whole_path)   

dir_name = input("Enter the database name : ")
file_name = input("Enter the file name : ")
dirpath = r"C:\Users\nsurisetti\Desktop"+'\\'+dir_name # Replace the "<dirpath>" with actual directory path.
file_path = directory_check(dirpath)+"\\"+file_name
whole_path = file_check(file_path)
print(whole_path)
ntree = ET.parse(whole_path)
root = ntree.getroot()

userinput=""
while userinput !="7":    
	userinput=Employee_menu()

	if userinput =="0":
		# dir_name = input("Enter the database name : ")
		# file_name = input("Enter the file name : ")
		# dirpath = r"C:\Users\nsurisetti\Desktop"+'\\'+dir_name # Replace the "<dirpath>" with actual directory path.
		# file_path = directory_check(dirpath)+"\\"+file_name
		print(file_check(file_path))

	if userinput =="1":
		create_employee()

	if userinput =="2":
		update_employee()

	if userinput =="3":
		delete_employee()

	if userinput =="4":
		search_for_an_employee()

	if userinput =="5":
		show_all_employees()
	
	if userinput=="6":validate(file_path)

print("Enter a valid input")
